package domain;

public class SecondSingleton {
    private static volatile SecondSingleton instance;

    public static SecondSingleton getInstance() {
        SecondSingleton localInstance = instance;
        if (localInstance == null) {
            synchronized (SecondSingleton.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new SecondSingleton();
                }
            }
        }
        return localInstance;
    }
}
